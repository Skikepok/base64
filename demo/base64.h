#ifndef BASE64_H
#define BASE64_H

/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */

#include <stdlib.h>
#include <string.h>

char *encode64 (const char *in, int length, int *res_len);
char *decode64 (const char *in, int length, int *res_len);

#endif /*   BASE64_H   */
