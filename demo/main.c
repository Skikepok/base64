#include <stdio.h>
#include <string.h>
#include "base64.h"

int main(void)
{
  char *str = "Hi, this is a demo.";
  char *encoded;
  int encoded_len;
  char *decoded;
  int decoded_len;
  
  encoded = encode64(str, strlen(str), &encoded_len);
  
  printf("64 encoding result: %.*s\n", encoded_len, encoded);

  decoded = decode64(encoded, strlen(encoded), &decoded_len);

  printf("64 decoding result: %.*s\n", decoded_len, decoded);

  printf("diff: %s", (!strcmp(decoded, str) ? "true" : "false"));

  free(encoded);
  free(decoded);
}
