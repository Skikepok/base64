/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */

#include "base64.h"

char encode64_char(int value)
{
  static const char *base =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

  return base[value];
}

char *encode64(const char *in, int length, int *res_len)
{
  int tmp_l = 0;
  int out_l = 0;

  int nb_malloc = length * 4 / 3 + 4;
  
  char *out = calloc (nb_malloc, 1);
  
  while (tmp_l <= length - 3)
  {
    out[out_l++] = encode64_char(in[tmp_l] >> 2);
    out[out_l++] = encode64_char(((in[tmp_l] & 3) << 4) + (in[tmp_l + 1] >> 4));
    out[out_l++] = encode64_char(((in[tmp_l + 1] & 15) << 2) + (in[tmp_l + 2] >> 6));
    out[out_l++] = encode64_char(in[tmp_l + 2] & 63);
    tmp_l += 3;
  }
  
  if (length - tmp_l)
  {
    out[out_l++] = encode64_char(in[tmp_l] >> 2);
    if (length - tmp_l == 1)
    {
      out[out_l++] = encode64_char((in[tmp_l] & 3) << 4);
      out[out_l++] = '=';
    }
    else
    {
      out[out_l++] = encode64_char(((in[tmp_l] & 3) << 4) + (in[tmp_l + 1] >> 4));
      out[out_l++] = encode64_char((in[tmp_l + 1] & 15) << 2);
    }
    out[out_l++] = '=';
  }
  
  out[out_l] = 0;
  
  if (res_len)
    *res_len = out_l;

  return out;
}

unsigned int decode_char(char value)
{
  if (value >= 'A' && value <= 'Z')
    return value - 'A';
  else if (value >= 'a' && value <= 'z')
    return 26 + (value - 'a');
  else if (value >= '0' && value <= '9')
    return 52 + (value - '0');
  else if (value == '+')
    return 62;
  else if (value == '/')
    return 63;
  else
    return 64;
}

char *decode64(const char *in, int length, int *res_len)
{
  int value[4];
  int out_l = 0;

  char *out = calloc (length * 3 / 4 + 2, 1);

  for (int i = 0; i < length; i++)
  {
    value[0] = decode_char(in[i++]);
    value[1] = decode_char(in[i++]);
    value[2] = decode_char(in[i++]);
    value[3] = decode_char(in[i]);

    out[out_l++] = (value[0] << 2) + (value[1] >> 4);
    if (value[2] != 64)
    {
      out[out_l++] = ((value[1] & 15) << 4) + (value[2] >> 2);
      if (value[3] != 64)
	out[out_l++] = ((value[2] & 3) << 6) + value[3];
    }
  }

  out[out_l] = 0;
  
  if (res_len)
    *res_len = out_l;

  return out;
}
